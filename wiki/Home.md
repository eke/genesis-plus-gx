Genesis Plus GX is an open-source Sega 8/16 bit emulator focused on accuracy and portability. Initially ported and developped on Gamecube / Wii consoles through [libogc / devkitPPC](http://sourceforge.net/projects/devkitpro/), this emulator is now available on many other platforms through various frontends such as:

* [Retroarch (libretro)](http://www.libretro.com)

* [Bizhawk](http://tasvideos.org/Bizhawk.html)

* [OpenEmu](http://openemu.org/)

----

The source code, originally based on Genesis Plus 1.2a by [Charles MacDonald](http://www.techno-junk.org/ ) has been heavily modified & enhanced, with respect to initial goals and design, in order to improve emulation accuracy as well as adding support for extra peripherals, cartridge or console hardware and many other exciting [features](https://bitbucket.org/eke/genesis-plus-gx/wiki/Features).

The result is that Genesis Plus GX is now more a continuation of the original project than a simple port, providing very accurate emulation and [100% compatibility](https://bitbucket.org/eke/genesis-plus-gx/wiki/Compatibility) with Genesis / Mega Drive, Sega/Mega CD, Master System, Game Gear & SG-1000 released software (including all unlicensed or pirate known dumps), also emulating backwards compatibility modes when available. All the people who contributed (directly or indirectly) to this project are listed on the [Credits](https://bitbucket.org/eke/genesis-plus-gx/wiki/Credits) page.

----

Multi-platform sourcecode (core) is available for use under a specific non-commercial [license](https://bitbucket.org/eke/genesis-plus-gx/src/aa9d0889309bc8b13f140be8949415a32d0461a7/LICENSE.txt) and maintained here as well as [github](https://github.com/ekeeke/Genesis-Plus-GX) so that other Genesis Plus ports can take advantage of it, as I really hope this emulator can become a reference for _open-source_ and _accurate_ Genesis emulation. If you ported this emulator to other platforms or need help porting it, feel free to contact me.

----

Latest official Gamecube / Wii standalone port (screenshots below) is available [here](https://bitbucket.org/eke/genesis-plus-gx/downloads). Be sure to check the included user manual first. A [startup guide](https://bitbucket.org/eke/genesis-plus-gx/wiki/Getting%20Started) and a [FAQ](https://bitbucket.org/eke/genesis-plus-gx/wiki/Frequently%20Asked%20Questions) are also available.

![MainMenu.png](https://bitbucket.org/repo/7AjE6M/images/3565283297-MainMenu.png)
![menu_load.png](https://bitbucket.org/repo/7AjE6M/images/164055790-menu_load.png)

![RomBrowser.png](https://bitbucket.org/repo/7AjE6M/images/1972035547-RomBrowser.png)
![CtrlMenu.png](https://bitbucket.org/repo/7AjE6M/images/2283464354-CtrlMenu.png)

----
You can also test latest compiled builds for some platforms by downloading them from [here](https://bitbucket.org/eke/genesis-plus-gx/src/710da247684c89ff3a3ee7f9d6785a96a01f0117/builds/).
